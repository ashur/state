const clone = require( "clone" );

class State
{
	/**
	 * @param {Object} options
	 * @param {Object} options.data
	 * @param {Object} options.computed
	 * @param {Function} options.mounted
	 * @param {Function} options.render
	 */
	constructor( options )
	{
		let state = this; // Make this.__data available to shim accessors

		/*
		 * Define data accessors
		 */
		this.__data = clone( options.data || {} ); // Raw values are stored here
		this.data = {}; // Shim object with getters and setters pointing to raw values

		Object.keys( this.__data ).forEach( property =>
		{
			Object.defineProperty( this.data, property, {
				get: function()
				{
					return state.__data[property];
				},
				set: function( value )
				{
					let newValue = value;
					if( typeof state.__data[property] === "object" )
					{
						if( Array.isArray( state.__data[property] ) )
						{
							newValue = [...value];
						}
						else
						{
							newValue = Object.assign( state.__data[property], value );
						}
					}
					state.__data[property] = newValue;

					state.render();
				}
			});
		});

		Object.keys( options.computed || [] ).forEach( property =>
		{
			if( this.data[property] )
			{
				throw new Error( `Cannot redefine '${property}' as a computed property` );
			}

			// Getter
			let getter = () =>
			{
				return state.__data[property];
			};

			if( typeof options.computed[property] === "function" )
			{
				getter = options.computed[property];
			}
			else if( typeof options.computed[property].get === "function" )
			{
				getter = options.computed[property].get;
			}

			// Setter
			let setter = value =>
			{
				let newValue = value;
				if( typeof state.__data[property] === "object" )
				{
					newValue = Object.assign( state.__data[property], value );
				}
				state.__data[property] = newValue;
			};

			if( typeof options.computed[property] === "function" )
			{
				setter = () =>
				{
					throw new Error( `Cannot set '${property}', a computed property defined as a function` )
				};
			}
			else if( typeof options.computed[property].get === "function" && options.computed[property].set === undefined )
			{
				setter = () =>
				{
					throw new Error( `Cannot set '${property}', a computed property defined as an object with no setter` )
				};
			}
			else if( typeof options.computed[property].set === "function" )
			{
				setter = options.computed[property].set;
			}

			Object.defineProperty( this.data, property, {
				get: getter,
				set: value =>
				{
					setter.call( state.data, value );
					state.render();
				},
			});
		});

		/*
		 * Define remaining internal properties
		 */
		if( options.mounted )
		{
			options.mounted.call( this.data );
		}

		this.__onRender = options.render;
		this.render();
	}

	/**
	 * Calls the instance's render method, if defined
	 */
	render()
	{
		if( this.__onRender && typeof this.__onRender === "function" )
		{
			this.__onRender.call( this.data );
		}
	}
}

module.exports.State = State;
