const {assert} = require( "chai" );
const {State} = require( "../" );

describe( "State", () =>
{
	context( ".constructor()", () =>
	{
		it( "should not throw if any properties are undefined", () =>
		{
			let fn = () =>
			{
				let state = new State({});
			};

			assert.doesNotThrow( fn );
		});

		it( "should throw Error if attempting to redefine a data property", () =>
		{
			let options = {
				data: {
					aString: "this is a string"
				},

				computed: {
					aString()
					{
						return "this is a computed string";
					},
				},
			};

			let fn = () => {
				let state = new State( options );
			};

			assert.throws( fn, "Cannot redefine 'aString' as a computed property" );
		});

		it( "should clone options.data rather than assign by reference", () =>
		{
			let defaultGreeting = "hello, world";
			let newGreeting = "hello, Dolly";

			let options = {
				data: {
					aString: defaultGreeting,
					anArray: [defaultGreeting],
					anObject: {
						aString: defaultGreeting,
					},
				},
			};

			let state = new State( options );

			state.data.aString = newGreeting;
			assert.equal( options.data.aString, defaultGreeting, "External 'options.data.aString'" );
			assert.equal( state.data.aString, newGreeting, "Internal 'data.aString'" );

			state.data.anArray.push( newGreeting );
			assert.deepEqual( options.data.anArray, [defaultGreeting], "External 'options.data.anArray'" );
			assert.deepEqual( state.data.anArray, [defaultGreeting, newGreeting], "Internal 'data.anArray'" );
		});
	});

	context( ".data", () =>
	{
		it( "should set properties declared by data property", () =>
		{
			let data = {
				aString: "this is a string",
				aBoolean: true,
				anObject: {
					aString: "this is another string",
					aBoolean: false,
					anObject: {
						aString: "this is the last string",
						aBoolean: true,
					},
				},
			};
	
			let state = new State({
				data: data,
			});
	
			assert.equal( state.data.aString, data.aString );
			assert.equal( state.data.aBoolean, data.aBoolean );
			assert.deepEqual( state.data.anObject, data.anObject);
		});

		it( "should support modifying arrays", () =>
		{
			let state = new State({
				data: {
					anArray: ["A", "B", "D"]
				},
			});

			state.data.anArray.pop();
			assert.deepEqual( state.data.anArray, ["A", "B"] );

			state.data.anArray.push( "C" );
			assert.deepEqual( state.data.anArray, ["A", "B", "C"] );

			state.data.anArray.sort( (a, b) => -1 );
			assert.deepEqual( state.data.anArray, ["C", "B", "A"] );
		});

		it( "should support redeclaring arrays", () =>
		{
			let state = new State({
				data: {
					anArray: ["A", "B", "C"]
				},
			});

			state.data.anArray = [];
			assert.deepEqual( state.data.anArray, [] );
		});
	});

	context( ".computed", () =>
	{
		it( "should return property values by calling computed property functions", () =>
		{
			let greeting = "hello, world.";

			let state = new State({
				computed: {
					greeting()
					{
						return greeting;
					}
				},
			});

			assert.equal( state.data.greeting, greeting );
		});

		it( "should return property values by calling computed property get() functions", () =>
		{
			let greeting = "hello, world.";

			let state = new State({
				computed: {
					greeting: {
						get()
						{
							return greeting;
						}
					}
				},
			});

			assert.equal( state.data.greeting, greeting );
		});

		it( "should set property values by calling computed property set() functions", () =>
		{
			let state = new State({
				data: {
					history: [],
					__value: "one",
				},

				computed: {
					value: {
						get()
						{
							return this.__value;
						},
						set( value )
						{
							this.history.push( this.__value );
							this.__value = value;
						}
					}
				},
			});

			assert.equal( state.data.value, "one" );
			assert.equal( state.data.history.length, 0 );
			state.data.value = "two";
			assert.equal( state.data.value, "two" );
			assert.equal( state.data.history.length, 1 );
		});

		it( "should throw Error when attempting to set computed property defined without setter", () =>
		{
			let state = new State({
				computed: {
					aFunction() {
						return "this is a function";
					},

					anObjectWithNoSetter: {
						get() {
							return "this is an object that does not define a setter"
						},
					}
				},
			});

			let fnSetFunction = () => state.data.aFunction = "a new string";
			let fnSetObjectWithNoSetter = () => state.data.anObjectWithNoSetter = "a new string";

			assert.throws( fnSetFunction, `Cannot set 'aFunction', a computed property defined as a function` );
			assert.throws( fnSetObjectWithNoSetter, `Cannot set 'anObjectWithNoSetter', a computed property defined as an object with no setter` );
		});
	});

	context( ".mounted", () =>
	{
		it( "should be called with 'this' context set to merged data object", () =>
		{
			return new Promise( resolve =>
			{
				let options = {
					data: {
						aString: "this is a string",
					},

					computed: {
						time()
						{
							return Date.now();
						},
					},

					mounted()
					{
						assert.equal( this.aString, options.data.aString, "this.aString" );
						assert.isNumber( this.time, "this.time" );

						resolve();
					},
				};

				let state = new State( options );
			});
		});
	});

	context( ".render", () =>
	{
		it( "should be called after .mounted()", () =>
		{
			return new Promise( resolve =>
			{
				let counter = 0;
				let options = {
					mounted()
					{
						counter++;
					},

					render()
					{
						assert.equal( counter, 1, "counter" );
						resolve();
					},
				};

				let state = new State( options );
			});
		});

		it( "should be called with 'this' context set to merged data object", () =>
		{
			return new Promise( resolve =>
			{
				let options = {
					data: {
						aString: "this is a string",
					},

					computed: {
						time()
						{
							return Date.now();
						},
					},

					render()
					{
						assert.equal( this.aString, options.data.aString, "this.aString" );
						assert.isNumber( this.time, "this.time" );

						resolve();
					},
				};

				let state = new State( options );
			});
		});

		it( "should be called each time a static property value is set", () =>
		{
			let counter = 0;

			let options = {
				data: {
					aBoolean: true,
				},

				render()
				{
					counter++;
				},
			};

			let state = new State( options );

			state.data.aBoolean = false;
			assert.equal( counter, 2, "counter" );

			state.data.aBoolean = true;
			assert.equal( counter, 3, "counter" );
		});

		it( "should be called each time a computed property value is set", () =>
		{
			let counter = 0;

			let options = {
				computed: {
					dynamicString: {
						get()
						{
							return "this is a computed property"
						},
						set( value )
						{
							// do something with `value` ...
						}
					}
				},

				render()
				{
					counter++;
				},
			};

			let state = new State( options );

			state.data.dynamicString = "this is a new string";
			assert.equal( counter, 2, "counter" );

			state.data.dynamicString = "this is a newer string";
			assert.equal( counter, 3, "counter" );
		});
	});
});
